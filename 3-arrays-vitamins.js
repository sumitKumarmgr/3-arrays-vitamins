const items = [{
    name: 'Orange',
    available: true,
    contains: "Vitamin C",
}, {
    name: 'Mango',
    available: true,
    contains: "Vitamin K, Vitamin C",
}, {
    name: 'Pineapple',
    available: true,
    contains: "Vitamin A",
}, {
    name: 'Raspberry',
    available: false,
    contains: "Vitamin B, Vitamin A",

}, {
    name: 'Grapes',
    contains: "Vitamin D",
    available: false,
}];




// 1. Get all items that are available 

const data = items.filter((itemData) => {
    if (itemData.available) {
        return true;
    }
})

console.log(data);


// 2. Get all items containing only Vitamin C.

const data2 = items.filter((itemData) => {
    if (itemData.contains === "Vitamin C") {
        return true;
    }
})
console.log(data2);






// 3. Get all items containing Vitamin A.

const data3 = items.filter((itemData) => {
    if (itemData.contains === "Vitamin A") {
        return true;
    }
})
console.log(data3);




// 4. Group items based on the Vitamins that they contain in the following format:
//     {
//         "Vitamin C": ["Orange", "Mango"],
//         "Vitamin K": ["Mango"],
//     }
//     and so on for all items and all Vitamins.

let data4 = items.reduce((accur, curr) => {
    curr.contains.split(', ').map((item) => {
        if (accur[item]) {
            accur[item] = [accur[item] + ", " + curr.name];
        } else {
            accur[item] = [curr.name];
        }
    });
    return accur;
}, {})

console.log(data4);








// 5. Sort items based on number of Vitamins they contain.

function vitaminsNo(items){
    return items.sort((item1,item2)=>item2['contains'].split('').length-item1['contains'].split('').length);
}
 console.log(items);
